#!/bin/bash

. ~/scripts/set-env-vars

run_shader_db() {
	if [ "$1" == "RV370" ]
	then
		GPUID="0x5460"
	elif [ "$1" == "RV530" ]
	then
		GPUID="0x71C0"
	fi

	cd ~/shader-db
	if [ -f "/tmp/shader-db-${1}.txt" ]
	then
		mv /tmp/shader-db-${1}.txt /tmp/shader-db-${1}.old
	fi
	LD_PRELOAD=$INSTALL/lib/libradeon_noop_drm_shim.so RADEON_GPU_ID=$GPUID ./run shaders > /tmp/shader-db-${1}.txt
	if [ -f "/tmp/shader-db-${1}.old" ]
	then
		DBRESULTS=$(./report.py /tmp/shader-db-${1}.old /tmp/shader-db-${1}.txt)
	fi
	cd -
}

COMMITS=$(git log --oneline origin/main.. | awk '{print $1}')
BRANCH=$(git rev-parse --abbrev-ref HEAD)

rm -rf /tmp/shader-db* 

git checkout origin/main
cd build
ninja install
cd ..

RESULTS=""
run_shader_db RV370
RESULTS="$DBRESULTS"
run_shader_db RV530
RESULTS=$(echo -e "Shader-db RV370:\n${RESULTS}\n\n Shader-db RV530:\n$DBRESULTS")

git checkout "$BRANCH"

git -c sequence.editor='sed -i s/pick/edit/' rebase -i origin/main

for c in $COMMITS
do
	cd build
	ninja install
	cd ..
	RESULTS=""
	run_shader_db RV370
	RESULTS="$DBRESULTS"
	run_shader_db RV530
	RESULTS=$(echo -e "Shader-db RV370:\n${RESULTS}\n\n Shader-db RV530:\n$DBRESULTS")
	OLD_MSG=$(git log --format=%B -n1)
	#git commit --amend -m"$OLD_MSG" -m"Signed-off-by: Pavel Onračka <pavel.ondracka@gmail.com>"
	git commit --amend -m"$OLD_MSG" -m"$RESULTS"
	git rebase --continue
done
