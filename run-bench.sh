#!/bin/bash
export vblank_mode=0
. ~/scripts/set-env-vars

if [ "$1" != "" ]
then
	cd ~/graphics/mesa/build
	git checkout $1
	ninja install
fi

cd ~/apps/sanctuary/
rm -f /tmp/frames-sanctuary.txt
for i in {1..5}
do
	echo
	./1280x800_fullscreen.sh | grep FPS | awk '{print $2}' >> /tmp/frames-sanctuary.txt
done

ministat -w 73 /tmp/frames-sanctuary.txt

cd ~/apps/Lightsmark2008.2.0/bin/pc-linux32/
rm -f /tmp/frames-lightsmark.txt

for i in {1..5}
do
	./backend 1280x800 fullscreen | grep "average fps" | awk '{print $5}' | sed 's/\.$//g' >> /tmp/frames-lightsmark.txt
done

ministat -w 73 /tmp/frames-lightsmark.txt
